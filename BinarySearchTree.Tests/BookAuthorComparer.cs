﻿using System;
using System.Collections.Generic;

namespace BinarySearchTree.Tests
{
    public class BookAuthorComparer : IComparer<Book>
    {
        public int Compare(Book x, Book y)
        {
            if (ReferenceEquals(x, y)) return 0;
            if (ReferenceEquals(null, y)) return 1;
            if (ReferenceEquals(null, x)) return -1;
            return string.Compare(x.Author, y.Author, StringComparison.Ordinal);
        }
    }
}