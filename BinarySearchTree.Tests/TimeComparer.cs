﻿using System.Collections.Generic;

namespace BinarySearchTree.Tests
{
    public class TimeComparer : IComparer<Time>
    {
        public int Compare(Time x, Time y)
        {
            int hoursComparison = x.Hours.CompareTo(y.Hours);
            return hoursComparison != 0 ? hoursComparison : x.Minutes.CompareTo(y.Minutes);
        }
    }
}