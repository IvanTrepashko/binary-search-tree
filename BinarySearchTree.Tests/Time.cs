﻿namespace BinarySearchTree.Tests
{
    /// <summary>
    /// Represents a struct for working with time in HH:MM format.
    /// </summary>
    public readonly struct Time
    {
        private const int MinutesInDay = 1440;
        private const int MinutesInHour = 60;
        private const int HoursInDay = 24;

        /// <summary>
        /// Initializes a new instance of the <see cref="Time"/> struct.
        /// </summary>
        /// <param name="minutes">Total minutes.</param>
        public Time(int minutes)
        {
            int totalMinutes = Time.CountMinutes(minutes);

            this.Hours = totalMinutes / Time.MinutesInHour;
            this.Minutes = totalMinutes % Time.MinutesInHour;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Time"/> struct.
        /// </summary>
        /// <param name="hours">Total hours.</param>
        /// <param name="minutes">Total minutes.</param>
        public Time(int hours, int minutes)
            : this(Time.CountMinutes(hours, minutes))
        {
        }

        /// <summary>
        /// Gets minutes of the current <see cref="Time"/> value.
        /// </summary>
        public int Minutes { get; }

        /// <summary>
        /// Gets hours of the current <see cref="Time"/> value.
        /// </summary>
        public int Hours { get; }

        /// <summary>
        /// Deconstructs current <see cref="Time"/> value.
        /// </summary>
        /// <param name="hours">Hours.</param>
        /// <param name="minutes">Minutes.</param>
        public void Deconstruct(out int hours, out int minutes)
        {
            hours = this.Hours;
            minutes = this.Minutes;
        }

        /// <summary>
        /// Deconstructs current <see cref="Time"/> value.
        /// </summary>
        /// <param name="minutes">Minutes.</param>
        public void Deconstruct(out int minutes) => minutes = this.Minutes + (this.Hours * 60);

        /// <summary>
        /// Converts <see cref="Time"/> value to its string representation.
        /// </summary>
        /// <returns>String representation of <see cref="Time"/> value.</returns>
        public new string ToString() => $"{this.Hours:d2}:{this.Minutes:d2}";

        private static int CountMinutes(int hours, int minutes) =>
            ((hours % Time.HoursInDay) * Time.MinutesInHour) + minutes;

        private static int CountMinutes(int minutes) =>
            (Time.MinutesInDay + (minutes % Time.MinutesInDay)) % Time.MinutesInDay;
    }
}