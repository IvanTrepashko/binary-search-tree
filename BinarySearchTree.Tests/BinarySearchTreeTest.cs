using System;
using NUnit.Framework;

namespace BinarySearchTree.Tests
{
    [TestFixture]
    public class BinarySearchTreeTest
    {
        [Test]
        public void BinaryTree_Integer_DefaultComparer_Preorder()
        {
            BinarySearchTree<int> tree = new BinarySearchTree<int>();
            
            tree.Add(10);
            tree.Add(-15);
            tree.Add(4);
            tree.Add(-1);
            tree.Add(8);

            int[] expected = {10, -15, 4, -1, 8};

            Assert.AreEqual(expected, tree.GetElementsPreorder());
        }

        [Test]
        public void BinaryTree_Integer_CustomComparer_Inorder()
        {
            BinarySearchTree<int> tree = new BinarySearchTree<int>(new IntegerAbsComparer());
            
            tree.Add(10);
            tree.Add(-15);
            tree.Add(4);
            tree.Add(-1);
            tree.Add(8);

            int[] expected = { -1, 4, 8, 10, -15 };

            var actual = tree.GetElementsInorder();
            
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BinaryTree_Book_DefaultComparer_Postorder()
        {
            Book book1 = new Book("Author1", "Title1", "Publisher1", "923-23-123");
            Book book2 = new Book("Author2", "Title2", "Publisher2", "423-23-123");
            Book book3 = new Book("Author3", "Title3", "Publisher3", "433-23-123");
            Book book4 = new Book("Author4", "Title4", "Publisher4", "523-23-123");
            Book book5 = new Book("Author5", "Title5", "Publisher5", "123-23-123");

            BinarySearchTree<Book> tree = new BinarySearchTree<Book>();
            
            tree.Add(book1);
            tree.Add(book2);
            tree.Add(book3);
            tree.Add(book4);
            tree.Add(book5);

            Book[] expected = {book5, book4, book3, book2, book1};
            
            Assert.AreEqual(expected, tree.GetElementsPostorder());
        }

        [Test]
        public void BinaryTree_Book_CustomComparer()
        {
            
            Book book1 = new Book("Author9", "Title1", "Publisher1", "923-23-123");
            Book book2 = new Book("Author5", "Title2", "Publisher2", "423-23-123");
            Book book3 = new Book("Author2", "Title3", "Publisher3", "433-23-123");
            Book book4 = new Book("Author6", "Title4", "Publisher4", "523-23-123");
            Book book5 = new Book("Author8", "Title5", "Publisher5", "123-23-123");

            BinarySearchTree<Book> tree = new BinarySearchTree<Book>(new BookAuthorComparer());
            
            tree.Add(book1);
            tree.Add(book2);
            tree.Add(book3);
            tree.Add(book4);
            tree.Add(book5);

            Book[] expected = { book3, book2, book4, book5, book1 };
            
            Assert.AreEqual(expected, tree.GetElementsInorder());
        }

        [Test]
        public void BinaryTree_String_DefaultComparer_Inorder()
        {
            string[] words = {"Word", "abc", "hhh", "ggg", "fff", "mmm"};

            BinarySearchTree<string> tree = new BinarySearchTree<string>();

            foreach (var word in words)
            {
                tree.Add(word);
            }

            string[] expected = {"abc", "fff", "ggg", "hhh", "mmm", "Word"};
            
            Assert.AreEqual(expected, tree.GetElementsInorder());
        }

        [Test]
        public void BinaryTree_String_CustomComparer_Preorder()
        {
            string[] words = {"aaaa", "aaa", "aaaaa", "aa", "aaaaaaa"};

            BinarySearchTree<string> tree = new BinarySearchTree<string>(new StringLengthComparer());

            foreach (var word in words)
            {
                tree.Add(word);
            }

            string[] expected = {"aaaa", "aaa", "aa", "aaaaa", "aaaaaaa"};
            
            Assert.AreEqual(expected, tree.GetElementsPreorder());
        }

        [Test]
        public void BinaryTree_Time_CustomComparer_Postorder()
        {
            Time time1 = new Time(15, 30);
            Time time2 = new Time(10, 40);
            Time time3 = new Time(10, 30);
            Time time4 = new Time(12, 45);
            Time time5 = new Time(21, 20);
            Time time6 = new Time(7, 10);

            BinarySearchTree<Time> tree = new BinarySearchTree<Time>(new TimeComparer());
            
            tree.Add(time1);
            tree.Add(time2);
            tree.Add(time3);
            tree.Add(time4);
            tree.Add(time5);
            tree.Add(time6);

            Time[] expected = { time6, time3, time4, time2, time5, time1 };
            
            Assert.AreEqual(expected, tree.GetElementsPostorder());
        }

        [Test]
        public void BinaryTree_GetEnumeratorTest()
        {
            BinarySearchTree<int> tree = new BinarySearchTree<int>(new IntegerAbsComparer());
            
            tree.Add(10);
            tree.Add(-15);
            tree.Add(4);
            tree.Add(-1);
            tree.Add(8);

            int[] expected = { -1, 4, 8, 10, -15 };
            
            Assert.AreEqual(expected, tree);
        }

        [Test]
        public void BinaryTree_ChangedDuringIteration_Throws_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var tree = new BinarySearchTree<int>(new IntegerAbsComparer())
                {
                    10,
                    -15,
                    4,
                    -1,
                    8
                };

                foreach (var unused in tree)
                {
                    tree.Add(10);
                }
            });
        }
    }
}