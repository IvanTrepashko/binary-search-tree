﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BinarySearchTree
{
    /// <summary>
    /// Represents binary search tree.
    /// </summary>
    /// <typeparam name="T">Type of data to store.</typeparam>
    public class BinarySearchTree<T> : IEnumerable<T>
    {
        private readonly IComparer<T> comparer;
        private TreeNode root;
        private int version;

        /// <summary>
        /// Creates new instance of <see cref="BinarySearchTree{T}"/> class.
        /// </summary>
        public BinarySearchTree()
        {
            this.version = 0;
            this.comparer = Comparer<T>.Default;
        }

        /// <summary>
        /// Creates new instance of <see cref="BinarySearchTree{T}"/> class.
        /// </summary>
        /// <param name="comparer">Comparer to order elements.</param>
        public BinarySearchTree(IComparer<T> comparer)
        {
            if (comparer is null)
            {
                this.comparer = Comparer<T>.Default;
            }

            this.version = 0;
            this.comparer = comparer;
        }

        /// <summary>
        /// Creates new instance of <see cref="BinarySearchTree{T}"/> class from IEnumerable source.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <exception cref="ArgumentNullException">Thrown when <param name="source"></param> is null.</exception>
        public BinarySearchTree(IEnumerable<T> source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            this.comparer = Comparer<T>.Default;

            foreach (var item in source)
            {
                this.Add(item);
            }

            this.version = 0;
        }

        /// <summary>
        /// Creates new instance of <see cref="BinarySearchTree{T}"/> class using source sequence and custom comparer.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <param name="comparer">Custom comparer.</param>
        /// <exception cref="ArgumentNullException">Thrown when source sequence is null.</exception>
        public BinarySearchTree(IEnumerable<T> source, IComparer<T> comparer)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            this.comparer = comparer ?? Comparer<T>.Default;

            foreach (var item in source)
            {
                this.Add(item);
            }

            this.version = 0;
        }

        /// <summary>
        /// Determines if given value is in the tree.
        /// </summary>
        /// <param name="value">Value to search for.</param>
        /// <returns>Result of searching.</returns>
        public bool Contains(T value)
        {
            return this.Contains(value, this.root);
        }

        /// <summary>
        /// Gets the elements of the tree in preorder.
        /// </summary>
        /// <returns>Elements of the tree.</returns>
        public IEnumerable<T> GetElementsPreorder()
        {
            List<T> result = new List<T>();
            
            this.GetElementPreorderRecursive(result, this.root);

            return result;
        }

        /// <summary>
        /// Gets the elements of the tree in postorder. 
        /// </summary>
        /// <returns>Elements of the tree.</returns>
        public IEnumerable<T> GetElementsPostorder()
        {
            List<T> result = new List<T>();

            this.GetElementsPostorderRecursive(result, this.root);

            return result;
        }

        /// <summary>
        /// Gets the elements of the tree in inorder.
        /// </summary>
        /// <returns>Elements of the tree.</returns>
        public IEnumerable<T> GetElementsInorder()
        {
            List<T> result = new List<T>();

            this.GetElementsInorderRecursive(result, this.root);

            return result;
        }

        /// <summary>
        /// Adds new item in the tree.
        /// </summary>
        /// <param name="data">Data to add.</param>
        public void Add(T data)
        {
            if (this.root is null)
            {
                this.root = new TreeNode(data);
            }
            else
            {
                root.Add(data, comparer);
            }

            this.version++;
        }
        
        /// <summary>
        /// Gets enumerator to iterate through binary search tree using inorder.
        /// </summary>
        /// <returns>An enumerator.</returns>
        /// <exception cref="ArgumentException">Thrown when binary search tree was changed during iteration.</exception>
        public IEnumerator<T> GetEnumerator()
        {
            int currentVersion = this.version;
            
            foreach (var item in GetElementsInorder())
            {
                if (this.version != currentVersion)
                {
                    throw new ArgumentException("Binary Search Tree cannot be changed during iteration.");
                }
                
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        private bool Contains(T value, TreeNode node)
        {
            if (node is null)
            {
                return false;
            }

            return this.comparer.Compare(value, node.Data) switch
            {
                0 => true,
                > 0 => this.Contains(value, node.RightNode),
                < 0 => this.Contains(value, node.LeftNode)
            };
        }
        
        private void GetElementsInorderRecursive(List<T> result, TreeNode node)
        {
            if (node is null)
            {
                return;
            }
            
            this.GetElementsInorderRecursive(result, node.LeftNode);
            
            result.Add(node.Data);
            
            this.GetElementsInorderRecursive(result, node.RightNode);
        }

        private void GetElementsPostorderRecursive(List<T> result, TreeNode node)
        {
            if (node is null)
            {
                return;
            }
            
            this.GetElementsPostorderRecursive(result, node.LeftNode);
            this.GetElementsPostorderRecursive(result, node.RightNode);

            result.Add(node.Data);
        }

        private void GetElementPreorderRecursive(List<T> result, TreeNode node)
        {
            if (node is null)
            {
                return;
            }

            result.Add(node.Data);
            this.GetElementPreorderRecursive(result, node.LeftNode);
            this.GetElementPreorderRecursive(result, node.RightNode);
        }
        
        /// <summary>
        /// Represents tree node.
        /// </summary>
        private class TreeNode
        {
            /// <summary>
            /// Gets data stored in a node.
            /// </summary>
            public T Data { get; private set; }
            
            /// <summary>
            /// Gets left node.
            /// </summary>
            public TreeNode LeftNode { get; private set; }
            
            /// <summary>
            /// Gets right node.
            /// </summary>
            public TreeNode RightNode { get; private set; }

            /// <summary>
            /// Creates new instance of <see cref="TreeNode"/> class.
            /// </summary>
            /// <param name="data">Data to store in a node.</param>
            public TreeNode(T data)
            {
                this.Data = data;
            }

            /// <summary>
            /// Adds new node.
            /// </summary>
            /// <param name="dataNode">Data to store in a node.</param>
            /// <param name="comparer">Comparer to order nodes.</param>
            public void Add(T dataNode, IComparer<T> comparer)
            {
                if (comparer.Compare(dataNode, this.Data) < 0)
                {
                    if (this.LeftNode is null)
                    {
                        this.LeftNode = new TreeNode(dataNode);
                    }
                    else
                    {
                        this.LeftNode.Add(dataNode, comparer);
                    }
                }

                if (comparer.Compare(dataNode, this.Data) > 0)
                {
                    if (this.RightNode is null)
                    {
                        this.RightNode = new TreeNode(dataNode);
                    }
                    else
                    {
                        this.RightNode.Add(dataNode, comparer);
                    }
                }

                if (comparer.Compare(dataNode, this.Data) == 0)
                {
                    this.Data = dataNode;
                }
            }
        }
    }
}