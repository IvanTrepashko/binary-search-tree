﻿using System;
using System.Collections.Generic;

namespace BinarySearchTree.Tests
{
    public class IntegerAbsComparer : Comparer<int>
    {
        public override int Compare(int x, int y)
        {
            return Math.Abs(x).CompareTo(Math.Abs(y));
        }
    }
}