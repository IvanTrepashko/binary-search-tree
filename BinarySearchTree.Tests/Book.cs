﻿using System;
using System.Globalization;

namespace BinarySearchTree.Tests
{
    /// <summary>
    /// Represents book.
    /// </summary>
    public class Book : IComparable<Book>, IEquatable<Book>
    {
        private bool published;
        private DateTime datePublished;
        private int totalPages;

        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        /// <param name="author">Author's name.</param>
        /// <param name="title">Book title.</param>
        /// <param name="publisher">Publisher.</param>
        public Book(string author, string title, string publisher)
        {
            this.Author = author;
            this.Title = title;
            this.Publisher = publisher;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        /// <param name="author">Author's name.</param>
        /// <param name="title">Book title.</param>
        /// <param name="publisher">Publisher.</param>
        /// <param name="isbn">ISBN code.</param>
        public Book(string author, string title, string publisher, string isbn)
            : this(author, title, publisher)
        {
            this.ISBN = isbn;
        }

        /// <summary>
        /// Gets the title of a book.
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Gets or sets total page count.
        /// </summary>
        public int Pages
        {
            get => this.totalPages;

            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value), "Pages count must be positive number.");
                }

                this.totalPages = value;
            }
        }

        /// <summary>
        /// Gets publisher of the book.
        /// </summary>
        public string Publisher { get; }

        /// <summary>
        /// Gets ISBN code.
        /// </summary>
        public string ISBN { get; }

        /// <summary>
        /// Gets author of the book.
        /// </summary>
        public string Author { get; }

        /// <summary>
        /// Gets price of the book.
        /// </summary>
        public double Price { get; private set; }

        /// <summary>
        /// Gets ISO Currency symbols.
        /// </summary>
        public string Currency { get; private set; }

        /// <summary>
        /// Marks book as published and assigns publishing date.
        /// </summary>
        /// <param name="datePublished">Publishing date.</param>
        public void Publish(DateTime datePublished)
        {
            this.published = true;
            this.datePublished = datePublished;
        }

        /// <summary>
        /// Gets publication date if book was published. Else returns "NYP".
        /// </summary>
        /// <returns>Publication date.</returns>
        public string GetPublicationDate()
        {
            if (this.published)
            {
                return this.datePublished.Date.ToString();
            }
            else
            {
                return "NYP";
            }
        }

        /// <summary>
        /// Sets price and ISO currency symbols of the book.
        /// </summary>
        /// <param name="price">Price of the book.</param>
        /// <param name="currency">ISO currency.</param>
        public void SetPrice(double price, RegionInfo currency)
        {
            this.Price = price;
            this.Currency = currency.ISOCurrencySymbol;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.Title} by {this.Author}";
        }

        public override int GetHashCode()
        {
            return (ISBN != null ? ISBN.GetHashCode() : 0);
        }

        public int CompareTo(Book other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return string.Compare(ISBN, other.ISBN, StringComparison.Ordinal);
        }

        public bool Equals(Book other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return ISBN == other.ISBN;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Book) obj);
        }
    }
}